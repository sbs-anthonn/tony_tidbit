import logging


class LoggingEnrichmentFilter(logging.Filter):
    """
    This is a filter which injects contextual information into the log.

    """

    def __init__(self, trace_id=None, special_data=None):
        super().__init__()
        self._trace_id = trace_id
        self._special_data = special_data

    def filter(self, record):
        record.trace_id = self._trace_id
        record.special_data = self._special_data
        return True
