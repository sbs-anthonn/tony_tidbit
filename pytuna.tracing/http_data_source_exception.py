from data_source_exception import DataSourceException


class HttpDataSourceException(DataSourceException):
    """ standard exception for http exceptions """

    def __init__(self, identifier, code):
        super().__init__(identifier)
        self.code = code

    @staticmethod
    def is_http_exception(exception):
        return True

    @staticmethod
    def from_http_exception(exception, error_category):
        code = exception['code']
        return HttpDataSourceException(error_category, code)
