class ExampleBuilder:
    """Builders equate to object transformers. EG: SL.Article -> schema.org.article.org"""

    def __init__(self):
        self._data = {
            'identifier': 'builder_identifier',
            'hero_image': 'http://sl.endpoint.example.com/hero/image.png',
            'referenced_images': ['http://sl.endpoint.example.com/ref/1.png',
                                  'http://sl.endpoint.example.com/ref/1.png'],
            'referenced_shortcodes': {
                '1': {
                    'from': '[img id=2121]',
                    'to': '<img href=....>'
                }
            }
        }

    def get_identifier(self):
        return self._data['identifier']

    def get_hero_image(self):
        return self._data['hero_image']

    def get_referenced_images(self):
        return self._data['referenced_images']

    def get_referenced_shortcodes(self):
        return self._data['referenced_shortcodes']

#
# Notes: Builder will have to remember what it converted and how.., alternative is single line messages
# for each conversion which makes machine readability harder
#
