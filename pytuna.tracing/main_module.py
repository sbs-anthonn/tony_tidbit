from urllib import request
import logging
import logging.config
from logging_enrichment_filter import LoggingEnrichmentFilter
from http_data_source_exception import HttpDataSourceException
from trace_builder import TraceBuilder
from example_builder import ExampleBuilder
from debug_logger import DebugLogger

#
# Notes:
# formatter options: https://hg.python.org/cpython/file/5c4ca109af1c/Lib/logging/__init__.py#l399
#
#
the_trace_id = 'my-trace-id'

LOGGING_CONFIGURATION = {
    'version': 1,
    'formatters': {
        'default': {
            'format': '%(levelname)-8s: %(trace_id)s special_data: %(special_data)s MSG: %(message)s'
        }
    },
    'filters': {
        'logging_enrichment_filter': {
            '()': LoggingEnrichmentFilter,
            'trace_id': the_trace_id,
            'special_data': 'my-special-data'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'filters': ['logging_enrichment_filter'],
            'formatter': 'default'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['console']
    },
}

# create logger with external configuration
logging.config.dictConfig(LOGGING_CONFIGURATION)

# case 1.       -- basic logging and message manipulation
#   Shows:
#       a. logging that is configured by a config that can be moved into a config module / file / env-var
#       b. filter which is used to inject external data into every message. - from a var and coded literal
#       c. shows that (potentially) more than one handler can be created and given the same log message
#
logging.info('log-message')
#
# result: INFO    : my-trace-id special_data: my-special-data MSG: log-message
#

# case 2.       -- exception hierarchy and handling
#   Shows:
#       * TBD
#
try:

    # ------------------------------------------------------------
    # this function is somewhere down in a service in our codebase
    def some_function_in_another_module():
        try:
            request.urlopen('http://bad-endpoint')
        except Exception as inner_exception:
            if HttpDataSourceException.is_http_exception(inner_exception):
                raise HttpDataSourceException.from_http_exception(inner_exception, '/router/cr-handle/not-found')
            raise
    # ------------------------------------------------------------

    # calling the function here that throws the wrapped exception
    some_function_in_another_module()

except HttpDataSourceException as outer_exception:
    logging.exception(outer_exception)


# case 3.       -- alternatives in treatment of halting conditions (Fail Fast)
#   Shows:
#       a. two choices on how we deal with halting conditions (assert or exception)
#
variable_external_data = 'found'
if variable_external_data == 'expected':
    print('i expected this to happen')
elif variable_external_data == 'found':
    print('i expected this to happen. found')
else:
    # assert False  - or
    raise Exception('I did not expect this')

# case 4.       -- tracing output that must be machine readable
#   Shows:
#       a. using trace classes to ensure that messages are formatted correctly for machine parsing
#
example_builder = ExampleBuilder()
TraceBuilder(logging)(example_builder)
# alternate syntax:
#   1. TraceBuilder(logging, example_builder)
#   2. TraceBuilder(example_builder)
#   3. @TraceBuilder on the ExampleBuilder class

#
# Result: INFO    : my-trace-id special_data: my-special-data MSG: Builder: builder_identifier
# http://sl.endpoint.example.com/hero/image.png ['http://sl.endpoint.example.com/ref/1.png',
# 'http://sl.endpoint.example.com/ref/1.png'] {'1': {'from': '[img id=2121]', 'to': '<img href=....>'}}
#


# case 4.       -- programmer debugging tools
#   Shows:
#       a. a debug assertion library for testing code path execution
#
some_tuple = {'silly', 'edward'}
DebugLogger(logging).dump(some_tuple)
DebugLogger(logging).assert_true(not True)
DebugLogger(logging).assert_true_l(lambda: 1 - 2 == 2)

# Maybe an alternate syntax wil metaclasses

# Result DEBUG   : my-trace-id special_data: my-special-data MSG: {'edward', 'silly'} DEBUG   : my-trace-id
# special_data: my-special-data MSG: Debug assertion failed. main_module.py <module> 114 ['DebugLogger(
# logging).assert_true(not True)\n'] DEBUG   : my-trace-id special_data: my-special-data MSG: Debug assertion failed.
# main_module.py <module> 115 ['DebugLogger(logging).assert_true_l(lambda: 1-2 == 2)\n']
