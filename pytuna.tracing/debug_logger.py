import inspect


class DebugLogger:
    """programmer code debug logging"""

    def __init__(self, logger):
        self._logger = logger

    def dump(self, target):
        self._logger.debug(target)

    def assert_true(self, expression):
        if not expression:
            self._logger.debug(
                'Debug assertion failed. %(filename)-s %(function)s %(lineno)s %(code_context)s',
                self._get_frame()
            )

    def assert_false(self, expression):
        if expression:
            self._logger.debug(
                'Debug assertion failed. %(filename)-s %(function)s %(lineno)s %(code_context)s',
                self._get_frame()
            )

    def assert_true_l(self, expression):
        if not expression():
            self._logger.debug(
                'Debug assertion failed. %(filename)-s %(function)s %(lineno)s %(code_context)s',
                self._get_frame()
            )

    def assert_false_l(self, expression):
        if expression():
            self._logger.debug(
                'Debug assertion failed. %(filename)-s %(function)s %(lineno)s %(code_context)s',
                self._get_frame()
            )

    @staticmethod
    def _get_frame():
        stack_frame = inspect.stack()[2]
        return {
            'filename': stack_frame.filename,
            'function': stack_frame.function,
            'lineno': stack_frame.lineno,
            'code_context': stack_frame.code_context
        }
