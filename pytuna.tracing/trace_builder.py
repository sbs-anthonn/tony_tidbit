from trace_base import TraceBase


class TraceBuilder(TraceBase):
    """Tracing for Builder modules. Builders equate to object transformers. EG: SL.Article -> schema.org.article.org"""
    def __init__(self, logger):
        super().__init__(logger)

    def __call__(self, builder):

        extended_logging = {
            'content_id': builder.get_identifier(),
            'hero_image': builder.get_hero_image(),
            'referenced_images': builder.get_referenced_images(),
            'referenced_shortcodes': builder.get_referenced_shortcodes()
        }

        self.logger.info(
            'Builder: %(content_id)-8s %(hero_image)s %(referenced_images)s %(referenced_shortcodes)s',
            extended_logging
        )
