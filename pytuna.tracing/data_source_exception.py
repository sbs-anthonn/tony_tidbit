class DataSourceException(Exception):
    """Base for all IO to datasources exceptions"""

    def __init__(self, identifier):
        self.identifier = identifier
