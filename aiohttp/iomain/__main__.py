import asyncio
import time
from urllib.request import urlopen
import aiohttp


async def fetch(session, url):
    async with session.get(url) as response:
        assert response.status == 200
        return await response.text()


async def main(urls):
    async with aiohttp.ClientSession() as session:
        tasks = []
        for url in urls:
            tasks.append(fetch(session, url))
        pages = await asyncio.gather(*tasks)
        for page in pages:
            print(len(page))
        return pages


if __name__ == '__main__':
    lookups = ['http://python.org', 'http://google.com', 'http://bing.com', 'https://aiohttp.readthedocs.io/en/stable/']

    #
    # case 1. serial lookups
    # - out of the box fetch for the page lookups above
    #
    start1 = time.perf_counter()

    for url in lookups:
        f = urlopen(url, None, 5)
        page = f.read()
        print(len(page))

    duration1 = time.perf_counter() - start1
    msg1 = 'serially: It took {:4.2f} seconds'
    print(msg1.format(duration1))

    #
    # case 2. parallel lookups
    # - use asyncio to run all the lookups (fetched) concurrently
    #
    start2 = time.perf_counter()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(lookups))

    duration2 = time.perf_counter() - start2
    msg2 = 'concurrently: It took {:4.2f} seconds'
    print(msg2.format(duration2))
