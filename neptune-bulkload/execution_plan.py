from collections import namedtuple


class ExecutionPlan:
    def __init__(self, serializer='text', writer='stdout'):
        self._serializer = serializer
        self._writer = writer

        # prod plan
        # self.plan = {
        #     'about': {
        #         'vertices': 100000,
        #         'edges': {}
        #     },
        #     'author': {
        #         'vertices': 1000,
        #         'edges': {}
        #     },
        #     'copyright_holder': {
        #         'vertices': 5,
        #         'edges': {}
        #     },
        #     'video': {
        #         'vertices': 500000,
        #         'edges': {}
        #     },
        #     'in_language': {
        #         'vertices': 1,
        #         'edges': {}
        #     },
        #     'publisher': {
        #         'vertices': 1,
        #         'edges': {
        #             'image': [1, 1]
        #         }
        #     },
        #     'image': {
        #         'vertices': 500000,
        #         'edges': {
        #             'copyright_holder': [1, 1]
        #         }
        #     },
        #     'article': {
        #         'vertices': 1000000,
        #         'edges': {
        #             'image': [1, 1],
        #             'video': [0, 2],
        #             'author': [1, 2],
        #             'in_language': [1, 5],
        #             'about': [15, 15],
        #             'publisher': [1, 1]
        #         }
        #     }
        # }

        # debug plan
        self.plan = {
            'about': {
                'vertices': 15,
                'edges': {}
            },
            'author': {
                'vertices': 10,
                'edges': {}
            },
            'copyright_holder': {
                'vertices': 5,
                'edges': {}
            },
            'video': {
                'vertices': 10,
                'edges': {}
            },
            'in_language': {
                'vertices': 1,
                'edges': {}
            },
            'publisher': {
                'vertices': 1,
                'edges': {
                    'image': [1, 1]
                }
            },
            'image': {
                'vertices': 10,
                'edges': {
                    'copyright_holder': [1, 1]
                }
            },
            'article': {
                'vertices': 100000,
                'edges': {
                    'image': [1, 1],
                    'video': [0, 2],
                    'author': [1, 2],
                    'in_language': [1, 5],
                    'about': [15, 15],
                    'publisher': [1, 1]
                }
            }
        }

    def update_plan(self, parameters):
        pass

    def serializer_type(self):
        return self._serializer

    def writer_type(self):
        return self._writer

    def get_vertex_plan(self):
        VertexPlan = namedtuple('VertexPlan', ['pool', 'size'])
        return [VertexPlan(i, self.plan[i]['vertices']) for i in self.plan.keys()]

    def get_edge_plan(self):
        EdgePlan = namedtuple('EdgePlan', ['from_pool', 'from_size', 'to_pool', 'to_size', 'sample_min', 'sample_max'])
        edges = []

        for v in self.plan:
            from_pool = v
            from_size = self.plan[v]['vertices']

            for e in self.plan[v]['edges']:
                to_pool = e
                to_size = self.plan[to_pool]['vertices']
                sam_min = self.plan[v]['edges'][e][0]
                sam_max = self.plan[v]['edges'][e][1]

                if sam_max < sam_min:
                    raise ValueError(f'Edge {e} for vertex {from_pool} is invalid. Max < min')

                if sam_min > to_size:
                    raise ValueError(
                        f'Edge {e} for vertex {from_pool} is invalid. sample size ({sam_min}) > pool size ({to_size})'
                    )

                edges.append(EdgePlan(from_pool, from_size, to_pool, to_size, sam_min, sam_max))

        return edges
