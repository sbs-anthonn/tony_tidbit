from collections.abc import Iterable


class SerializerText:
    def __init__(self, pool):
        self._pool = pool
        self._iter = iter(pool)
        self._attribs = None

    def __iter__(self):
        return self

    def __next__(self):
        return self.__serialize(next(self._iter))

    def get_iterator(self):
        return self.__iter__()

    def __serialize(self, model):
        result = ''
        if self._attribs is None:
            self._attribs = self.__get_model_attributes(model)
            header_names = '\t'.join(self._attribs) + '\n'
            keyword_rename = header_names.replace('_', '')
            keyword_rename = keyword_rename.replace('identifier', 'id')
            result += keyword_rename

        for d in self.__get_values_in_header_order(self._attribs, model):
            if isinstance(d, Iterable) and not isinstance(d, str):
                result += f'\t[{",".join(d)}]'
            else:
                result += f'\t{d}'

        return result

    @staticmethod
    def __get_model_attributes(model):
        attrib = set([a for a in dir(model) if not a.startswith('__') and not callable(getattr(model, a))])
        return attrib.difference({'type', 'context'})

    @staticmethod
    def __get_values_in_header_order(names, model):
        return [model.__dict__[i] for i in names]
