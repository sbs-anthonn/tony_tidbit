from factory import Factory
from execution_plan import ExecutionPlan
import numpy as np


def main():

    plan = ExecutionPlan('csv', 'file')
    plan.update_plan({})

    # create a record-set for each vertex configured in the execution plan
    for v in plan.get_vertex_plan():
        pool = Factory.pool(v.pool, v.size)
        serializer = Factory.serializer(plan.serializer_type(), pool)
        writer = Factory.writer(plan.writer_type(), serializer, f'vertex-{v.pool}')
        writer.write_all()

    # create a record-set for each edge configured in the execution plan
    for e in plan.get_edge_plan():
        pool_from = Factory.pool(e.from_pool, e.from_size)
        pool_to = Factory.pool(e.to_pool, e.to_size)
        pool = Factory.edge_pool(pool_from, pool_to, e.sample_min, e.sample_max)
        serializer = Factory.serializer(plan.serializer_type(), pool)
        writer = Factory.writer(plan.writer_type(), serializer, f'edge-{e.from_pool}-{e.to_pool}')
        writer.write_all()


if __name__ == "__main__":
    main()
