class ImageModel:
    def __init__(self):  # edges, copyright_holder):
        self.context = "http://schema.org"
        self.type = "ImageObject"
        self.identifier = "08e231bc-c74f-40f1-aa7c-b2d2167f8fbc"
        self.name = "20191120001433634256-original.jpg"
        self.dateCreated = "2019-11-19T23:57:43+00:00"
        self.dateModified = "2019-11-19T23:57:43+00:00"

        self.url = "https://sl.sbs.com.au/public/image/file/08e231bc-c74f-40f1-aa7c-b2d2167f8fbc"

        self.alternativeHeadline = "Police arrest a protester after he tried to escape from Hong Kong Polytechnic "
        self.contentSize = 5409406
        self.caption = "Police arrest a protester after he tried to escape from Hong Kong Polytechnic University  "

        # edges
        # self.copyrightHolder = copyright_holder
