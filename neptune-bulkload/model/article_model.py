class ArticleModel:
    def __init__(self):  # edges, images, videos, authors, cr_holders, in_langs, abouts, publishers):
        self.context = "http://schema.org"
        self.type = "Article"
        self.identifier = "ecfc67b5-4faf-41ac-b486-ad024f3a2efb"
        self.name = "US Senate unanimously passes Hong Kong human rights bill"
        self.description = "The US Senate has passed legislation aimed at protecting the human rights of Hong Kong's "
        self.url = "http://www.sbs.com.au/news/article/2019/11/19/us-senate-unanimously-passes-hong-kong-human-rights" \
                   "-bill"
        self.headline = "US Senate unanimously passes Hong Kong human rights bill"
        self.creativeWorkStatus = "Published"
        self.dateCreated = "2019-11-19T06:45:12+00:00"
        self.dateModified = "2019-11-20T01:38:03+00:00"
        self.datePublished = "2019-11-19T06:45:12+00:00"
        self.articleBody = "<p>"

        # edges
        # self.image = images
        # self.video = videos
        # self.author = authors
        # self.copyrightHolder = cr_holders
        # self.inLanguage = in_langs
        # self.about = abouts
        # self.publisher = publishers

