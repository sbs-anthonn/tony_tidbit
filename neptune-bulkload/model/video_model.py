class VideoModel:
    def __init__(self):
        self.context = "http://schema.org"
        self.type = "VideoObject"
        self.identifier = "ecfc67b5-4faf-41ac-b486-ad024f3a2efb", "mpx://v/1631640131748"
        self.name = "Burnout - Preview"
        self.description = "Burnout isn’t just a response to working long hours. It’s feelings of exhaustion"
        self.thumbnailUrl = "https://videocdn-sbs.akamaized.net/u/video/SBS/managed/images/2019/10/28" \
                            "/1631640131748_10281010_image102946_large.jpg "
        self.expires = "2029-12-31T13:00:00Z"
        self.duration = "PT52S"
        self.uploadDate = "2019-10-27T09:30:00Z"

        # strange but doesnt have edge to copyright holder atm
