import importlib
from data.edge_pool import EdgePool
from serializer_csv import SerializerCsv
from serializer_text import SerializerText
from writer_std_out import WriterStdOut
from writer_file import WriterFile


class Factory:

    @staticmethod
    def pool(stype, size):
        module_name = f'data.{stype}_pool'
        module = importlib.import_module(module_name)
        class_name = ''.join(x.title() for x in stype.split('_')) + 'Pool'
        class_ = getattr(module, class_name)
        return class_(size)

    @staticmethod
    def edge_pool(pool_from, pool_to, sample_min, sample_max):
        return EdgePool(pool_from.get_size(), pool_from, pool_to, sample_min, sample_max)

    @staticmethod
    def serializer(stype, pool):
        if stype.lower() == 'csv':
            return SerializerCsv(pool)
        return SerializerText(pool)

    @staticmethod
    def writer(stype, serializer, pool):
        if stype == 'stdout':
            return WriterStdOut(serializer)
        return WriterFile(serializer, pool)
