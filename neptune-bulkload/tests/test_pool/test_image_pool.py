import unittest
from data.image_pool import ImagePool


class TestAboutPool(unittest.TestCase):

    def test_pool(self):
        pool = ImagePool(20, {})
        for x in pool:
            print(f'{x.name} - {x.caption}')

    def test_pool_random_group(self):
        pool = ImagePool(20000, {})
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.caption}')
