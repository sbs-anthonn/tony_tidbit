import unittest
from data.about_pool import AboutPool


class TestAboutPool(unittest.TestCase):

    def test_pool(self):
        pool = AboutPool(20)
        for x in pool:
            print(f'{x.name} - {x.termCode}')

    def test_pool_random_group(self):
        pool = AboutPool(20000)
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.termCode}')
