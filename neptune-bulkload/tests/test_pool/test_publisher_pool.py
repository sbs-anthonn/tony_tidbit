import unittest
from data.publisher_pool import PublisherPool


class TestAboutPool(unittest.TestCase):

    def test_pool(self):
        pool = PublisherPool(20)
        for x in pool:
            print(f'{x.name} - {x.identifier}')

    def test_pool_random_group(self):
        pool = PublisherPool(20000)
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.identifier}')
