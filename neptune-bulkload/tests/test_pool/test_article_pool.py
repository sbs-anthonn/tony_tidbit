import unittest
from data.article_pool import ArticlePool


class TestArticlePool(unittest.TestCase):

    def test_pool(self):
        pool = ArticlePool(20)
        for x in pool:
            print(f'{x.name} - {x.identifier} - {x.description}')

    def test_pool_random_group(self):
        pool = ArticlePool(20000)
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.identifier} - {x.description}')
