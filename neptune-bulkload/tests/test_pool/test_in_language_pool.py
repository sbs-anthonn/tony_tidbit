import unittest
from data.in_language_pool import InLanguagePool


class TestInLanguagePool(unittest.TestCase):

    def test_pool(self):
        pool = InLanguagePool()
        for x in pool:
            print(f'{x.name} - {x.identifier} - {x.alternateName}')

    def test_pool_random_group(self):
        pool = InLanguagePool()
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.identifier} - {x.alternateName}')
