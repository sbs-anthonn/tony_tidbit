import unittest
from data.copyright_holder_pool import CopyrightHolderPool


class TestAboutPool(unittest.TestCase):

    def test_pool(self):
        pool = CopyrightHolderPool(20)
        for x in pool:
            print(f'{x.name} - {x.identifier} - {x.type}')

    def test_pool_random_group(self):
        pool = CopyrightHolderPool(20000)
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.identifier} - {x.type}')
