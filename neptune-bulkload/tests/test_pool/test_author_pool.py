import unittest
from data.author_pool import AuthorPool


class TestAboutPool(unittest.TestCase):

    def test_pool(self):
        pool = AuthorPool(20)
        for x in pool:
            print(f'{x.name} - {x.identifier}')

    def test_pool_random_group(self):
        pool = AuthorPool(20000)
        group = pool.select_random_group(15)
        for x in group:
            print(f'{x.name} - {x.identifier}')
