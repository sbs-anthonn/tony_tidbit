from model.about_model import AboutModel
from data.object_pool import ObjectPool, ObjectPoolIterator

_TERM_TYPES = [
    'Organization',
    'Person',
    'Ingredient',
    'Website',
    'Country',
    'Region',
    'Sports',
    'News',
    'Fashion',
    'Recipe'
]


class AboutPool(ObjectPool):
    def __init__(self, pool_size):
        self._size = pool_size

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('AboutPool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        model = AboutModel()
        model.name = f"Term-{ordinal}"
        model.termCode = _TERM_TYPES[self.generate_random_ordinal(len(_TERM_TYPES))]
        model.identifier = self.pool_item_identifier(ordinal)
        return model
