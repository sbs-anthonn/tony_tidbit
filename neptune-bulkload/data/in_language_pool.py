from model.in_language_model import InLanguageeModel
from data.object_pool import ObjectPool, ObjectPoolIterator


class InLanguagePool(ObjectPool):
    def __init__(self, *unused):
        self._size = len(_LANG_TYPES)

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('InLanguagePool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        model = InLanguageeModel()
        model.name = f"Language-{ordinal}"
        model.identifier = self.pool_item_identifier(ordinal)
        model.alternateName = _LANG_TYPES[ordinal]
        return model


_LANG_TYPES = [
    "af",
    "aii",
    "am",
    "ar",
    "bg",
    "bn",
    "bo",
    "bs",
    "cnh",
    "cs",
    "da",
    "de",
    "din",
    "el",
    "en",
    "es",
    "et",
    "fa",
    "fi",
    "fil",
    "fj",
    "fr",
    "gu",
    "he",
    "hi",
    "hmv",
    "hr",
    "hu",
    "hy",
    "id",
    "it",
    "ja",
    "kar",
    "km",
    "kn",
    "ko",
    "ku",
    "lo",
    "lt",
    "lv",
    "mk",
    "ml",
    "mn",
    "ms",
    "mt",
    "my",
    "ne",
    "nl",
    "nn",
    "pa",
    "pl",
    "prs",
    "ps",
    "pt",
    "rar",
    "rhg",
    "rn",
    "ro",
    "ru",
    "si",
    "sk",
    "sl",
    "sm",
    "so",
    "sq",
    "sr",
    "sv",
    "sw",
    "ta",
    "te",
    "th",
    "ti",
    "to",
    "tr",
    "uk",
    "und",
    "ur",
    "vi",
    "yi",
    "zh-hans",
    "zh-hant"
]
