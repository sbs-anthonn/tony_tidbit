from model.copyright_holder_model import CopyrightHolderModel
from data.object_pool import ObjectPool, ObjectPoolIterator


_HOLDER_TYPES = [
    'Organization',
    'Person'
]


class CopyrightHolderPool(ObjectPool):
    def __init__(self, pool_size):
        self._size = pool_size

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('CopyrightHolderPool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        model = CopyrightHolderModel()
        model.name = f"Copyright-Holder-{ordinal}"
        model.identifier = self.pool_item_identifier(ordinal)
        model.type = _HOLDER_TYPES[self.generate_random_ordinal(len(_HOLDER_TYPES))]
        return model
