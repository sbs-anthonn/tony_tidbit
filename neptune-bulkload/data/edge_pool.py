from model.edge_model import EdgeModel
from data.object_pool import ObjectPool, EdgePoolIterator


class EdgePool(ObjectPool):
    def __init__(self, pool_size, from_pool=None, to_pool=None, sample_min=0, sample_max=0):
        self._unused = pool_size
        self._size = from_pool.get_size()
        self._from_pool = from_pool
        self._to_pool = to_pool
        self._sample_min = sample_min
        self._sample_max = sample_max
        self._to_pool_name = self.__lower_first(type(to_pool).__name__.replace('Pool', ''))

    def __iter__(self):
        return EdgePoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        pass

    def select_random_group(self, group_size):
        return []

    def select_random_group_identifiers(self, group_size):
        return []

    def __generate_item(self, ordinal):

        # the exec plan allows variable number of edges between 0 - max edge pool size
        # sometimes the range is set as a non variable number [x,x]. calc how many edge to make
        if self._sample_max == self._sample_min:
            number_samples = self._sample_max
        else:
            number_samples = self.generate_random_integer(self._sample_min, self._sample_max)

        if number_samples == 0:
            return []

        from_model_id = self._from_pool.pool_item_identifier(ordinal)
        to_sample_ids = self._to_pool.select_random_group_identifiers(number_samples)

        edges = []
        for e in to_sample_ids:
            model = EdgeModel()
            model.identifier = str(self.generate_identifier(f'{from_model_id}-{e}', 0))
            model.label = self._to_pool_name
            model._from = from_model_id
            model.to = e
            edges.append(model)

        return edges

    @staticmethod
    def __lower_first(s):
        return  s[:1].lower() + s[1:] if s else ''
