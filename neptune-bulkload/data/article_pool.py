from model.article_model import ArticleModel
from data.object_pool import ObjectPool, ObjectPoolIterator

_MAX_TEXT_LEN = 20
_LAST_2_YEARS = 365 * 2

_PUB_STATES = [
    'Published',
    'Unpublished'
]


class ArticlePool(ObjectPool):
    def __init__(self, pool_size, images=None, videos=None, authors=None, cr_holders=None, in_langs=None,
                 abouts=None, publishers=None):
        self._size = pool_size
        self.images = images
        self.videos = videos
        self.authors = authors
        self.cr_holders = cr_holders
        self.in_langs = in_langs
        self.abouts = abouts
        self.publishers = publishers

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('ArticlePool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        identifier = self.pool_item_identifier(ordinal)
        dates = self.generate_ascending_dates_string(_LAST_2_YEARS, 3)
        model = ArticleModel()
        model.name = f"Article-{ordinal}"
        model.identifier = identifier
        model.description = f'Article-{ordinal} {self.generate_random_text(_MAX_TEXT_LEN)}'
        model.url = f'http://www.sbs.com.au/news/article/{model.description.replace(" ", "-")}'
        model.headline = f'Article-{ordinal}-Headline {self.generate_random_text(_MAX_TEXT_LEN)}'
        model.creativeWorkStatus = _PUB_STATES[self.generate_random_ordinal(len(_PUB_STATES))]
        model.dateCreated = dates[0]
        model.dateModified = dates[1]
        model.datePublished = dates[2]
        # think not storing body - model.articleBody = self.generate_random_text(_MAX_TEXT_LEN)
        return model
