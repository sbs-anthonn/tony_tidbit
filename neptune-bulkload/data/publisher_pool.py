from model.publisher_model import PublisherModel
from data.object_pool import ObjectPool, ObjectPoolIterator


class PublisherPool(ObjectPool):
    def __init__(self, pool_size, logo_image=None):
        self._size = pool_size
        self._logo_image = logo_image

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('PublisherPool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        model = PublisherModel()
        model.identifier = self.pool_item_identifier(ordinal)
        model.name = f"Publisher-SBS-{ordinal}"
        return model
