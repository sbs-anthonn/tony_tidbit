from collections.abc import Iterable
from abc import ABC, abstractmethod
import numpy as np
import uuid
import lorem
from collections import deque
from datetime import datetime, timedelta
import random


class ObjectPool(ABC):

    @abstractmethod
    def __iter__(self):
        while False:
            yield None

    def get_iterator(self):
        return self.__iter__()

    @abstractmethod
    def get_size(self):
        pass

    @abstractmethod
    def pool_item(self, ordinal):
        pass

    @abstractmethod
    def pool_item_identifier(self, ordinal):
        pass

    @abstractmethod
    def select_random_group(self, group_size):
        pass

    @abstractmethod
    def select_random_group_identifiers(self, group_size):
        pass

    @staticmethod
    def generate_identifier(tag, ordinal):
        return uuid.uuid5(uuid.NAMESPACE_URL, tag + str(ordinal) + '.sbs.com.au')

    @staticmethod
    def generate_random_text(max_length):
        return lorem.sentence()[0:max_length]

    @staticmethod
    def generate_random_ordinal(max_value):
        # return np.random.randint(low=0, high=max_value, size=1)[0].item()
        return int(max_value * random.random())

    @staticmethod
    def generate_random_integer(min_value, max_value):
        # return np.random.randint(low=min_value, high=max_value+1, size=1)[0]
        num = int(max_value * random.random())
        while num < min_value:
            num = int(max_value * random.random())
        return num

    @staticmethod
    def generate_ascending_dates(within_days, count):
        # offsets = sorted(np.random.randint(low=2, high=within_days, size=count), reverse=True)
        offsets = []
        for i in range(count):
            offsets.append(ObjectPool.generate_random_integer(2, within_days))

        result = sorted(offsets)
        return [datetime.today() - timedelta(days=i) for i in result]

    @staticmethod
    def generate_ascending_dates_string(within_days, count):
        dates = ObjectPool.generate_ascending_dates(within_days, count)
        return [i.strftime('%Y-%m-%dT%H:%M:%S') + 'Z' for i in dates]

    @staticmethod
    def generate_random_sample(sample_size, max_ordinal):
        if sample_size > max_ordinal:
            raise ValueError('Cant generate sample greater than pool size')

        return np.random.choice(max_ordinal, sample_size, replace=False)


class ObjectPoolIterator:
    def __init__(self, max_size, target):
        self._current = 0
        self._max_size = max_size
        self._target = target

    def __next__(self):
        if self._current >= self._max_size:
            raise StopIteration

        item = self._target.pool_item(self._current)
        self._current += 1

        return item


class EdgePoolIterator:
    def __init__(self, max_size, target):
        self._max_size = max_size
        self._target = target
        self._edges = SimpleQueue()
        self._from_iter = iter(range(0, max_size))

    def __next__(self):
        if self._max_size == 0:
            raise StopIteration

        # Edge pool is different to other pools. It returns an array of edges instead of a single
        # item like all the other pools. So, in this iterator, we need to remember the array of edges and
        # feed them back one at a time until we run out, then we move to the next item in the from pool and
        # start again

        while not self.__refill_item_cache():
            pass

        return self._edges.pop()

    def __refill_item_cache(self):
        # if not empty, do nothing
        if not self._edges.is_empty():
            return True

        # if empty, try and refill. may be that some calls to edge pool return no edges, so move to next item
        # in from pool until all 'from' are exhausted
        while True:
            ordinal = self._from_iter.__next__()
            edges = self._target.pool_item(ordinal)
            self._edges.push(edges)

            if not self._edges.is_empty():
                return True


class SimpleQueue:
    def __init__(self):
        self._store = deque()

    def is_empty(self):
        return len(self._store) == 0

    def pop(self):
        if self.is_empty():
            return None
        return self._store.popleft()

    def push(self, data):
        if isinstance(data, Iterable):
            for i in data:
                self._store.append(i)
        else:
            self._store.append(data)
