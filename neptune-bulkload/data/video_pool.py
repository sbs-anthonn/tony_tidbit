from model.video_model import VideoModel
from data.object_pool import ObjectPool, ObjectPoolIterator

_MAX_TEXT_LEN = 80


class VideoPool(ObjectPool):
    def __init__(self, pool_size):
        self._size = pool_size

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('VideoPool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        identifier = self.pool_item_identifier(ordinal)
        model = VideoModel()
        model.name = f"Video-{ordinal}"
        model.identifier = identifier  # , f"mpx://v/{identifier}"]
        model.description = f'Video-{ordinal} {self.generate_random_text(_MAX_TEXT_LEN)}'
        model.thumbnailUrl = f'https://videocdn-sbs.akamaized.net/u/video/SBS/managed/{identifier}.jpg'
        model.expires = '2029-12-31T13:00:00Z'
        model.duration = 'PT52S'
        model.uploadDate = "2019-10-27T09:30:00Z"
        return model

