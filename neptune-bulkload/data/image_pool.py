from model.image_model import ImageModel
from data.object_pool import ObjectPool, ObjectPoolIterator

_MAX_TEXT_LEN = 80
_LAST_2_YEARS = 365 * 2


class ImagePool(ObjectPool):
    def __init__(self, pool_size, copyright_holder=None):
        self._size = pool_size
        self._copyright_holder = copyright_holder

    def __iter__(self):
        return ObjectPoolIterator(self._size, self)

    def get_size(self):
        return self._size

    def pool_item(self, ordinal):
        return self.__generate_item(ordinal)

    def pool_item_identifier(self, ordinal):
        return str(self.generate_identifier('ImagePool', ordinal))

    def select_random_group(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.__generate_item(i) for i in samples]

    def select_random_group_identifiers(self, group_size):
        samples = self.generate_random_sample(group_size, self._size)
        return [self.pool_item_identifier(i) for i in samples]

    def __generate_item(self, ordinal):
        identifier = self.pool_item_identifier(ordinal)
        dates = self.generate_ascending_dates_string(_LAST_2_YEARS, 2)
        model = ImageModel()
        model.identifier = identifier
        model.name = f'{identifier}-original.jpg'
        model.dateCreated = dates[0]
        model.dateModified = dates[1]
        model.url = f'https://sl.sbs.com.au/public/image/file/{identifier}'
        model.alternativeHeadline = f'Image-{ordinal} {self.generate_random_text(_MAX_TEXT_LEN)}'
        model.contentSize = self.generate_random_ordinal(1000000)
        model.caption = f'Image-{ordinal} {self.generate_random_text(_MAX_TEXT_LEN)}'
        return model
