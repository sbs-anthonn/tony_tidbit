from collections.abc import Iterable

_DATA_TYPE_MAP = {
    int: 'Int',
    str: 'String',
    'date': 'Date'
}

_IGNORE_COLUMN_LIST = [
    'context'
]

_DATE_COLUMN_LIST = [
    'dateCreated',
    'dateModified',
    'datePublished',
    'expires',
    'uploadDate'
]

_SYSTEM_COLUMN_LIST = [
    'id',
    'label',
    'from',
    'to',
    'identifier',
    '_from',
    'type'
]

_RENAME_ATTRIBUTE_MAP = {
    'identifier': '~id',
    '_from': '~from',
    'type': '~label',
    'label': '~label',
    'to': '~to'
}


class SerializerCsv:
    def __init__(self, pool):
        self._pool = pool
        self._iter = iter(pool)
        self._attribs = None
        self._attrib_dt = {}

    def __iter__(self):
        return self

    def __next__(self):
        return self.__serialize(next(self._iter))

    def get_iterator(self):
        return self.__iter__()

    def __serialize(self, model):
        result = ''
        if self._attribs is None:
            self._attribs = self.__get_model_attributes(model)
            self._attrib_dt = self.__classify_attrib_data_types(self._attribs, model)

            for a in self._attribs:
                if a in _SYSTEM_COLUMN_LIST:
                    result += self.__get_renamed_attribute(a)
                    result += ','
                else:
                    result += self.__get_renamed_attribute(a)
                    result += ':'
                    result += self._attrib_dt.get(a, "String")
                    result += ','

            result = result[:-1] + '\n'

        for d in self.__get_values_in_header_order(self._attribs, model):
            if isinstance(d, Iterable) and not isinstance(d, str):
                result += f',{",".join(d)}],'
            else:
                result += f'{d},'

        return result[:-1]

    @staticmethod
    def __get_model_attributes(model):
        return [a for a in dir(model) if not a.startswith('__') and not callable(getattr(model, a))
                and a not in _IGNORE_COLUMN_LIST]

    @staticmethod
    def __classify_attrib_data_types(attribs, model):
        result = {}
        for a in attribs:
            if a in _SYSTEM_COLUMN_LIST:
                result[a] = None
            elif a in _DATE_COLUMN_LIST:
                result[a] = _DATA_TYPE_MAP.get('date', 'Date')
            else:
                data = model.__dict__[a]
                type_of = type(data)
                result[a] = _DATA_TYPE_MAP.get(type_of, 'String')

        return result

    @staticmethod
    def __get_renamed_attribute(attrib):
        renamed = _RENAME_ATTRIBUTE_MAP.get(attrib, None)
        return renamed if renamed else attrib

    @staticmethod
    def __get_values_in_header_order(names, model):
        return [model.__dict__[i] for i in names]
