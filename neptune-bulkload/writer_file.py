import os


class WriterFile:
    def __init__(self, serializer, pool):
        self.serializer = serializer
        self.pool = pool

    def write_all(self):
        my_file = f'./output/{self.pool}-neptune.csv'

        try:
            os.remove(my_file)
        except OSError:
            pass

        with open(my_file, "a") as f:
            for i in self.serializer:
                f.write(i)
                f.write('\r\n')
