class ServiceLayerThingAdapter:
    def __init__(self, payload):
        self._payload = payload

    def get_identifier(self):
        return self._payload["identifier"]

    def get_name(self):
        return self._payload["name"]

    def get_description(self):
        return self._payload["description"]

    def get_url(self):
        return self._payload["url"]
