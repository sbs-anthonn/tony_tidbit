class ArticleBuilder:
    def __init__(self, article_adapter):
        self._adapter = article_adapter

    def build(self):
        # question of model is does this return the json or does it return a model
        # assume json / dict
        return {
            "content": {
                "@context": "http://schema.org",
                "@type": "Article",
                "identifier": [
                    self._adapter.get
                ],
                "name": "More migrants than ever crossing the US border this year. What changed?",
                "description": "Figures released suggest President Trump's measure to stop migrants crossing the "
                               "border are failing",
                "url": "http://www.stg.sbs.com.au/news/article/2019/03/06/more-migrants-ever-crossing-us-border-year"
                       "-what-changed",
                "headline": "More migrants than ever crossing the US border this year. What changed?",
                "creativeWorkStatus": "Published",

            }
