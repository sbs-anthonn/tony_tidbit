from service.thing.thing_adapter import ServiceLayerThingAdapter


class CreativeWorkAdapter(ServiceLayerThingAdapter):
    def __init__(self, payload):
        super().__init__(payload)
        self._payload = payload

    def get_headline(self):
        return self._payload['headline']

    def get_creative_work_status(self):
        return self._payload['creativeWorkStatus']

    def get_date_created(self):
        return self._payload['dateCreated']

    def get_date_modified(self):
        return self._payload['dateModified']

    def get_date_published(self):
        return self._payload['datePublished']

    def get_copyright_year(self):
        return self._payload['copyrightYear']

    def get_expiration_date(self):
        return self._payload['expires']

    def get_copyright_holder(self):
        return self._payload['copyrightHolder']
