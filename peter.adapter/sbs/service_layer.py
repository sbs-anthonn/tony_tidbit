from sbs.resolver_interface import Resolver


class ServiceLayer(Resolver):
    def __init__(self, env):
        self.env = env

    def resolve_article(self, uuid):
        print(self.env, uuid)
        return {
            "body": "body",
            "headline": "headline",
            "images": ["Image1", "Image2"],
            "videos": ["Video1", "Video2"],
            "authors": ["Author1", "Author2"],
            "url": "http://me",
            "languages": ["Language1", "Language2"]
        }
