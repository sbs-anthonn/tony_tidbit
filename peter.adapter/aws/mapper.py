import os
from sbs.service_layer import ServiceLayer
from service.article.article_adapter import ServiceLayerArticleAdapter
from service.article.article_builder import ServiceLayerArticleBuilder

def my_handler(event, context):
    # get all configuration, env vars, ssm parameters
    env = os.get_environment("ENV")

    # determine if in lambda or ALB mode (assume route in api-gw or ald path /v1/{type}/{id})
    is_lambda = True
    thing_type = 'article'
    thing_id = 'uuid_example'

    if is_lambda:
        thing_type = event["pathParameters"]["type"]
        thing_id = event["pathParameters"]["id"]

    try:
        if thing_type is "article":
            payload = ServiceLayer(env).resolve_article(thing_id)
            adapter = ServiceLayerArticleAdapter(payload)
            output  = ServiceLayerArticleBuilder(adapter)


    except Exception:
        return {"RFC 7807 - special error payload object"}
