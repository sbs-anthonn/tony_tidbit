import logging

# create logger

module_logger = logging.getLogger(__name__)


class SubSubModule:

    def log_hello(self):
        print("My name is "  + __name__)
        module_logger.info('sub sub module hello: ' + __name__)
