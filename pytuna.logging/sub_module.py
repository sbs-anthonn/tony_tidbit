import logging
from child.sub_sub_module import SubSubModule

# create logger
module_logger = logging.getLogger(__name__)


class SubModule:

    def log_hello(self):
        module_logger.info('sub module hello: ' + __name__)

    def sub_sub_log_hello(self):
        ssm = SubSubModule()
        ssm.log_hello()
