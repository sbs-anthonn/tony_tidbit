import logging
from sub_module import SubModule
from child.sub_sub_module import SubSubModule
from non_getlogger import non_configured_logger


# case 0. use out of box logging
logging.error('case 0: default logger out of box')

# create logger with 'spam_application'
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# case 1. main module uses the logger configured locally
logger.info('main_module saying hello')

# case 2. sub module (local) passes its messages up to logger configured here in main
sm = SubModule()
sm.log_hello()

# case 3. child.sub_sub_module created locally passes messages up to logger configured in main
ssm = SubSubModule()
ssm.log_hello()

# case 4. sub_module calls sub_sub_module which logs, passing messages up to logger configured in main
sm.sub_sub_log_hello()

# case 5. default (root) logger levels affected by setting root logger settings in this file
logging.getLogger().setLevel(logging.INFO)
non_configured_logger()

